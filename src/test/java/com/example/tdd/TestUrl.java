package com.example.tdd;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestUrl {
    @Test
    public void testUrlConstructorDomain(){
        Url test = new Url("https://www.google.com/dog");
        assertEquals("google.com",test.getDomain());
        assertEquals("/dog",test.getPath());
        assertEquals("https",test.getProtocol());
    }

    @Test
    public void testCorrectCase(){
        Url test = new Url("HTTPS://WWW.GOOGLE.COM/");
        assertEquals("google.com",test.getDomain());
        assertEquals("/",test.getPath());
        assertEquals("https",test.getProtocol());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProtocolForOkFormat(){
        Url test = new Url("xtp://www.launchcode.org/");
        fail("Baaaaad protocol");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyDomain() {
        Url test = new Url("http//www.launchcode.org/");
        fail("No domain. May be missing : in url.");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyProtocol() {
        Url test = new Url("://www.launchcode.org/");
        fail("No protocol found.");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadDomainChar() {
        Url test = new Url("http://www.launc?hcode.org/");
        fail("Bad chars in the domain");
    }

    @Test
    public void testUrlToStringOverride(){
        Url test = new Url("http://google.org/");
        assertEquals("http://google.org/",test.toString());
    }



}
