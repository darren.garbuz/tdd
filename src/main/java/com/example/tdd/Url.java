package com.example.tdd;

import java.util.Arrays;

public class Url {
    final private String domain;
    final private String protocol;
    final private String path;

    public Url(String url) {
        //Define the good domains
        String[] goodDoms = {"http", "https", "ftp","file"};

        url = url.toLowerCase();
        //Build some string to construct the props
        StringBuilder pro =new StringBuilder("");
        StringBuilder pth = new StringBuilder("");
        StringBuilder dom = new StringBuilder("");
        //Loop through the url until we hit the :
        for(int i = 0; i < url.length(); i++){
            if(url.charAt(i)!=':'){ pro.append(url.charAt(i)); }
            else{ break; }
        }

        if(pro.toString() == "") {
            throw new IllegalArgumentException("No protocol found! Please recheck url format.");
        }
        if(!Arrays.asList(goodDoms).contains(pro.toString())) {
            throw new IllegalArgumentException("Please use valid protocol!");
        }

        //Clear out protocol and www
        url = url.replaceFirst(pro.toString(),"");
        url = url.replaceFirst("://","");
        if(url.startsWith("www.")){url = url.replaceFirst("www.","");}

        //Build up the domain
        for(int j = 0; j < url.length(); j++){
            if(url.charAt(j) != '/' ){ dom.append(url.charAt(j)); }
            else{break;}
        }

        if(dom.toString() == "") {
            throw new IllegalArgumentException("No domain found! Please recheck to see if there is a : in the protocol");
        }

        if(!dom.toString().matches("^[a-zA-Z0-9_.-]*$")) {

            throw new IllegalArgumentException("Domain may only have num, let, _, -, and .");
        }

        //Get rid of domain from url
        url = url.replaceFirst(dom.toString(),"");
        //Everything else goes to path
        pth.append(url);


        this.domain = dom.toString();
        this.protocol = pro.toString();
        this.path = pth.toString();
    }

    public String getDomain() {
        return domain;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getPath() {
        return path;
    }

    public String toString(){
        return this.getProtocol() + "://" + this.getDomain() + this.getPath();
    }

}
